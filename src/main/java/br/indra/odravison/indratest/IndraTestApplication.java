package br.indra.odravison.indratest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IndraTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(IndraTestApplication.class, args);
    }

}
